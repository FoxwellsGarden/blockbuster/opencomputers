local comp = require("component")
local fs = require("filesystem")
local shell = require("shell")
local args, _ = shell.parse(...)
local tape = comp.tape_drive
local tape_size = tape.getSize()
local pos = 0
local blocksize = 1024 * 8
local rnd = fs.open(args[1], "r")
print("Rewinding...")
tape.seek(-math.huge)

while pos < tape_size do
    tape.write(string.rep(string.char(0), blocksize))
    pos = pos + blocksize
end

tape.seek(-math.huge)
pos = 0

while pos < tape_size do
    local data = rnd:read(blocksize)
    if data == nil then break end
    tape.write(data)
    pos = pos + blocksize
    print(((pos / tape_size) * 100) .. " % written")
end

print("Rewinding...")
tape.seek(-math.huge)
tape.setLabel(args[1])