--[[
TODO: auto eject drives somehow (debug card?)
--]]
local computer=require("computer")
local event=require("event")
local os=require("os")

while true do
    local _,id,ev_type = event.pull("component_added")
    if ev_type ~= "filesystem" then
        return
    end
    print("install --to="..id.." --noreboot")
    computer.beep()
    computer.beep()
    computer.beep()
end