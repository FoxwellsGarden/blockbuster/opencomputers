local internet = require("internet")
local shell = require("shell")
local text = require("text")
local inspect = require("inspect")
local args = shell.parse(...)

if #args ~= 2 then
    io.write("usage: getter <url> <file>\n")

    return
end

local handle = internet.request(args[1], nil, {
    ["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.132 Safari/537.36"
}, "GET")

local result = ""
local downloaded = 0
local f = io.open(shell.resolve(text.trim(args[2])), "a")

for chunk in handle do
    local mt = getmetatable(handle)
    local code, message, headers = mt.__index.response()
    downloaded = downloaded + #chunk
    io.write("Size: " .. downloaded .. " bytes")
    if headers["Content-Length"] ~= nil then
        io.write(" (" .. math.floor((downloaded / headers["Content-Length"][1]) * 100) .. "%)")
    end
    io.write("\n")
    f:write(result)
end

f:close()
io.write("Done.\n")