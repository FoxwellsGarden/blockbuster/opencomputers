# compute

Set of scripts to run Lua programs at a large scale for computing purposes

## setup

1. Install `server.lua` onto one main machine and make sure it can wirelessly communicate with others
2. Install `manager.lua` onto one machine representing each "group", making sure it can communicate with the server and the workers, and setting its hostname to the desired ID
3. Install `worker.lua` onto several machines connected to the manager, and make sure they can't broadcast wirelessly (will cause issues)
4. Using the server, write/modify the script to send Lua files that return one value and see the workers run it
