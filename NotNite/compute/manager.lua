local component = require("component")
local modem = component.modem
local event = require("event")
local serialization = require("serialization")
modem.open(7070) -- Server <-> manager port
modem.open(7080) -- Manager <-> worker port
local workerCode = ""
local workers = {}
local jobs = {}

local function jobuuid()
    local random = math.random
    local template = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx"

    return string.gsub(template, "[xy]", function(c)
        local v = (c == "x") and random(0, 0xf) or random(8, 0xb)

        return string.format("%x", v)
    end)
end

local function sendJob(worker)
    local id = jobuuid()
    jobs[id] = workerCode

    modem.send(worker, 7080, serialization.serialize({
        type = "WorkerJob",
        code = workerCode,
        job = id
    }))
end

local function updateWorkers()
    for k, v in pairs(workers) do
        sendJob(v)
    end
end

while true do
    local ev = {event.pull()}

    if ev[1] == "modem_message" then
        local data = serialization.unserialize(ev[6])

        if ev[4] == 7070.0 then
            if data.type == "WorkerCodeUpdate" and data.subject == os.getenv("HOSTNAME") then
                workerCode = data.code
                updateWorkers()
            end
        elseif ev[4] == 7080.0 then
            if data.type == "WorkerRegister" then
                table.insert(workers, ev[3])
                sendJob(ev[3])
            elseif data.type == "JobDone" then
                modem.broadcast(7070, serialization.serialize({
                    type = "JobResult",
                    job = data.job,
                    worker = ev[3],
                    result = data.result
                }))

                jobs[data.job] = nil
            end
        end
    end
end