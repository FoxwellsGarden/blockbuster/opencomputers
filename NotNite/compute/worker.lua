local component = require("component")
local modem = component.modem
local event = require("event")
local serialization = require("serialization")
modem.open(7080) -- Manager <-> worker port

modem.broadcast(7080, serialization.serialize({
    type = "WorkerRegister"
}))

while true do
    local ev = {event.pull()}

    if ev[1] == "modem_message" then
        local data = serialization.unserialize(ev[6])

        if ev[4] == 7080.0 and data.type == "WorkerJob" then
            local jobresult = load(data.code)()

            modem.send(ev[3], 7080, serialization.serialize({
                type = "JobDone",
                job = data.job,
                result = jobresult
            }))
        end
    end
end