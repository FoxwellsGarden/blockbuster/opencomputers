local component = require("component")
local modem = component.modem
local serialization = require("serialization")
local event = require("event")
io.write("Enter code: ")
local scriptcode = io.read()
io.write("Enter ID: ")
local id = io.read()

modem.open(7070)
modem.broadcast(7070, serialization.serialize({
    type = "WorkerCodeUpdate",
    subject = id,
    code = scriptcode
}))

while true do
    local ev = {event.pull()}
    if ev[1] == "modem_message" then
        local data = serialization.unserialize(ev[6])
        if data.type == "JobResult" then
            io.write("job " .. data.job .. " finished\nresult: " .. data.result .. "\n")
        end
    end
end