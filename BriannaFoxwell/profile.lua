local shell = require("shell")
local tty = require("tty")
local fs = require("filesystem")
local computer = require("computer")

if tty.isAvailable() then
  if io.stdout.tty then
    io.write("\27[40m\27[37m")
    tty.clear()
  end
end
-- dofile("/etc/motd")

local seconds = math.floor(computer.uptime())
local minutes, hours =0, 0
if seconds >= 60 then
  minutes = math.floor(seconds/60)
  seconds = seconds % 60
end
if minutes >= 60 then
  hours = math.floor(minutes/60)
  minutes = minutes % 60
end


print('\27[32m     #     #                                   ')
print('     #     # #####  ####  #    # #    #  ####  ')
print('     #     #   #   #      #    # #    # #    # ')
print('     #     #   #    ####  #    # ###### #    # ')
print('     #     #   #        # #    # #    # #    # ')
print('     #     #   #   #    # #    # #    # #    # ')
print('      #####    #    ####   ####  #    #  ####  ')
print('                                         ')
print('     ######                              ')
print('     #     #  ####   ####  #    #  ####  ')
print('     #     # #    # #    # #   #  #      ')
print('     ######  #    # #      ####    ####  ')
print('     #   #   #    # #      #  #        # ')
print('     #    #  #    # #    # #   #  #    # ')
print('     #     #  ####   ####  #    #  ####  ')
print(' ')
print('Uptime: ' .. string.format("%02d:%02d:02d\n", hours, minutes, seconds))

shell.setAlias("dir", "ls")
shell.setAlias("move", "mv")
shell.setAlias("rename", "mv")
shell.setAlias("copy", "cp")
shell.setAlias("del", "rm")
shell.setAlias("md", "mkdir")
shell.setAlias("cls", "clear")
shell.setAlias("rs", "redstone")
shell.setAlias("view", "edit -r")
shell.setAlias("help", "man")
shell.setAlias("cp", "cp -i")
shell.setAlias("l", "ls -lhp")
shell.setAlias("..", "cd ..")
shell.setAlias("df", "df -h")
shell.setAlias("grep", "grep --color")
shell.setAlias("more", "less --noback")
shell.setAlias("reset", "resolution `cat /dev/components/by-type/gpu/0/maxResolution`")

os.setenv("EDITOR", "/bin/edit")
os.setenv("HISTSIZE", "10")
os.setenv("HOME", "/home")
os.setenv("IFS", " ")
os.setenv("MANPATH", "/usr/man:.")
os.setenv("PAGER", "less")
os.setenv("PS1", "\27[40m\27[32mutsuho@utsuho.rocks:$PWD$ \27[37m")
os.setenv("LS_COLORS", "di=0;36:fi=0:ln=0;33:*.lua=0;32")

shell.setWorkingDirectory(os.getenv("HOME"))

local home_shrc = shell.resolve(".shrc")
if fs.exists(home_shrc) then
  loadfile(shell.resolve("source", "lua"))(home_shrc)
end
