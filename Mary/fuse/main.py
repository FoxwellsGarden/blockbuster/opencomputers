#!/usr/bin/env python3
from fuse import FUSE, Operations, LoggingMixIn, FuseOSError
import socket
import logging
from stat import S_IFDIR, S_IFLNK, S_IFREG
from errno import ENODATA, EINVAL
import math
import threading

class Response():
  def __init__(self, code, data):
    self.code = code
    self.data = data
    self.length = len(data)

def clean(path):
  if str.endswith(path, "/"):
    return path[0:-1]
  else:
    return path

class OpenComputers(LoggingMixIn, Operations):
  def __init__(self, address, port):
    self.connections = {}
    self.fd = 0
    self.address = address
    self.port = int(port)

  @property
  def connection(self):
    ident = threading.get_ident()
    try:
      return self.connections[ident]
    except KeyError:
      print("Opening new connection! " + str(ident))
      self.connections[ident] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.connections[ident].connect((self.address, self.port))
      return self.connections[ident]

  def request(self, method, data=b""):
    print("Making request: " + method)
    command = bytes(method, "utf8") + bytes(16 - len(method))
    self.connection.send(command + len(data).to_bytes(8, byteorder="little") + data)
    codeBytes, address = self.connection.recvfrom(8)
    if len(codeBytes) != 8:
      raise Exception("Empty code bytes!")
    code = int.from_bytes(codeBytes, "little") # errno
    lengthBytes, address = self.connection.recvfrom(8)
    length = int.from_bytes(lengthBytes, "little")
    # Unsure if this should be done like this...
    print("Receiving " + str(length) + " bytes FOR:" + method)
    if length == 0:
      # self.connection.close()
      # del self.connections[threading.get_ident()]
      return Response(code, bytes(0))
    else:
      buffer = bytes(0)
      chunk_size = 256
      while length > 0:
        data, address = self.connection.recvfrom(min(length, 256))
        if len(data) == 0:
          raise Exception("No data?")
        length -= len(data)
        buffer += data
      assert(length == 0)
      # self.connection.close()
      # del self.connections[threading.get_ident()]
      return Response(code, buffer)

  def chmod(self, path, mode):
    return 0

  def chown(self, path, uid, gid):
    return 0

  def create(self, path, mode):
    # Touch
    req = self.request("touch", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)
    else:
      self.fd += 1
      return self.fd

  def getattr(self, path, fh=None):
    req = self.request("stat", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)
    size = int(int.from_bytes(req.data[0:8], "little"))
    lastModified = int(int.from_bytes(req.data[8:16], "little") / 1000)
    mode = req.data[16]
    return {
      "st_mode": (mode << 9) | 0o777,
      "st_nlink": 1,
      "st_size": size,
      "st_ctime": lastModified,
      "st_mtime": lastModified,
      "st_atime": lastModified,
    }

  def getxattr(self, path, name, position=0):
    raise FuseOSError(ENODATA)

  def listxattr(self, path):
    return []

  def mkdir(self, path, mode):
    req = self.request("mkdir", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def open(self, path, flags):
    self.fd += 1
    return self.fd
  
  def read(self, path, size, offset, fh):
    req = self.request("read", size.to_bytes(8, byteorder="little") + offset.to_bytes(8, byteorder="little") + bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)
    return req.data

  def readdir(self, path, fh):
    req = self.request("readdir", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)
    results = [".", ".."] + [clean(path.decode("utf8")) for path in req.data.split(b"\0")]
    return results

  def readlink(self, path):
    req = self.request("readlink", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSErorr(req.code)
    elif req.length == 0:
      raise FuseOSError(EINVAL) 
    return req.data

  def removexattr(self, path, name):
    raise FuseOSError(ENODATA)

  def rename(self, old, new):
    req = self.request("rename", bytes(old, "utf8") + bytes(1) + bytes(new, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def rmdir(self, path):
    req = self.request("rmdir", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def setxattr(self, path):
    raise FuseOSError(ENOTSUP)

  def statfs(self, path):
    req = self.request("statfs", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

    total = int.from_bytes(req.data[8:16], "little") / 512
    used = int.from_bytes(req.data[0:8], "little") / 512
    print(str(total) + " used: " + str(used))
    return {
      "f_bsize": 512,
      "f_blocks": int(total),
      "f_bavail": int(total - used),
      "f_bfree": int(total - used),
    }

  def symlink(self, target, source):
    req = self.request("link", bytes(old, "utf8") + bytes(1) + bytes(new, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def truncate(self, path, length, fh=None):
    req = self.request("truncate", length.to_bytes(8, byteorder="little") + bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def unlink(self, path):
    req = self.request("unlink", bytes(path, "utf8"))
    if req.code != 0:
      raise FuseOSError(req.code)

  def utimens(self, path, times=None):
    pass # Not able to change time in OC

  def write(self, path, data, offset, fh):
    self.request("write", offset.to_bytes(8, byteorder="little") + bytes(path, "utf8") + bytes(1) + data)
    return len(data)

class the():
  def __init__(self):
    self.contents = jhe()

class jhe():
  def __init__(self):
    self.fh = None
  
if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("mount")
  parser.add_argument("address")
  parser.add_argument("port")
  args = parser.parse_args()

  logging.basicConfig(level=logging.DEBUG)
  fuse = FUSE(OpenComputers(args.address, args.port), args.mount, foreground=True, allow_other=False, nothreads=True)
