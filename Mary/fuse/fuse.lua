local component = require("component")
local filesystem = require("filesystem")
local io = require("io")
local thread = require("thread")

--[[local thread = {
  create = function(fn, ...) fn(...) end
}--]]

thread.create(function()
  local server, err = component.compute_card.bind(5158)
  if server == nil then
    return error(err)
  end
  local clients = {}
  _G.fuseRunning = true -- Allow the service to be killed externally
  while _G.fuseRunning do
    local client = server.accept()
    if client == nil then
      os.sleep(0)
      goto continue
    end
    print("New connectioN", client)
    local clientIndex = table.insert(clients, client)
    thread.create(function(client)
      local runningClient = true -- Easiest way to deal with read() errors
  
      local function encode_int(val)
        --print("Encode?", val)
        assert(val  < 3220822564990)
        return string.char(val & 0xff) .. string.char((val >> 8) & 0xff)
          .. string.char((val >> 16) & 0xff) .. string.char((val >> 24) & 0xff)
          -- upper 32 bits
          .. string.char((val >> 32) & 0xff) .. string.char((val >> 40) & 0xff)
          .. string.char((val >> 48) & 0xff) .. string.char((val >> 56) & 0xff)
      end
      local function decode_int(val)
        return string.byte(val, 1) | (string.byte(val, 2) << 8)
          | (string.byte(val, 3) << 16) | (string.byte(val, 4) << 24)
          | (string.byte(val, 5) << 32) | (string.byte(val, 6) << 40)
          | (string.byte(val, 7) << 48) | (string.byte(val, 8) << 56)
      end
      assert(true)
      local enc = encode_int(1024)
      print(decode_int(encode_int(1024)), string.byte(enc, 1), string.byte(enc, 2), string.byte(enc, 3), string.byte(enc, 4), string.byte(enc, 5), string.byte(enc, 6), string.byte(enc,7), string.byte(enc, 8))
      assert(decode_int(encode_int(1024563)) == 1024563)
      local function read(n)
        local buffer = ""
        while #buffer ~= n do
          local tmp, err = client.read(n - #buffer)
          if tmp == "" then
            os.sleep(0)
          elseif tmp == nil then
            print(err)
            runningClient = false
            return buffer
          else
            local intendedRead = n - #buffer
            buffer = buffer .. tmp
            assert(#buffer <= n, "Buffer size mismatch! Reading:" .. n .. " sizeof buffer " .. #buffer .. " we read: " .. #tmp .. " intended read: ", intendedRead)
          end
        end
        return buffer
      end
      local function send(code, data)
        if data == nil then
          data = ""
        end
        client.write(encode_int(code) .. encode_int(#data) .. data)
      end
      local function unpad(chars)
        for i=1,#chars do
          if string.sub(chars, i, i) == "\0" then
            return string.sub(chars, 1, i-1)
          end
        end
        return chars
      end
      while runningClient do
        local command = unpad(read(16))
        local len = read(8) -- Size of data packet
        local data = read(decode_int(len))
        if command == "statfs" then
          local fs = filesystem.get(data)
          send(0, encode_int(fs.spaceUsed()) .. encode_int(fs.spaceTotal()))
        elseif command == "stat" then
          local exists = filesystem.exists(data)
          if not exists then
            send(2)
          else
            local size = filesystem.size(data)
            local modified = filesystem.lastModified(data)
            local isDir = filesystem.isDirectory(data)
            local isLink = filesystem.isLink(data)
            local mode = 0
            if isLink then
              mode = 80 -- S_IFLNK
            elseif isDir then
              mode = 32 -- S_IFDIR
            else
              mode = 64 -- S_IFREG
            end
            send(0, encode_int(size) .. encode_int(modified) .. string.char(mode))
          end
        elseif command == "readdir" then
          local files = filesystem.list(data)
          local data = ""
          for file in files do
            data = data .. file .. "\0"
          end
          if #data == 0 then
            send(2)
          else
            data = string.sub(data, 1, #data-1)
            send(0, data)
          end
        elseif command == "read" then
          --print("<-", command, data)
          --print(#string.sub(data, 1, 8))
          local size = decode_int(string.sub(data, 1, 8))
          local offset = decode_int(string.sub(data, 9, 16))
          local path = string.sub(data, 17)
          --print(size, offset, path, "H!")
          local file, err = io.open(path, "rb")
          if file == nil then
            print("No such file in read", path)
            send(2)
          else
            if offset ~= 0 then
              print("Seek read", offset, file:seek("set", offset))
            end
            local buffer = ""
            while #buffer < size do
              local data, err = file:read(size - #buffer)
              if data == "" then
                os.sleep(0)
              elseif data == nil then
                print("Data was nil?!")
                break
              else
                buffer = buffer .. data
              end
            end
            file:close()
            print("Size:", #buffer, size)
            assert(#buffer <= size)
            send(0, buffer)
            ---error("Hi")
          end
        elseif command == "mkdir" then
          local path = data
          local ok, err = filesystem.makeDirectory(path)
          if ok then
            send(0)
          else
            send(2) -- ENOENT
          end
        elseif command == "touch" then
          local file, err = io.open(data, "a")
          if file == nil then
            print("Error touching", data, err)
            file:close()
            send(1) -- EPERM
          else
            file:close()
            send(0)
          end
        elseif command == "truncate" then
          local length = decode_int(data)
          local path = string.sub(data, 9)
          if filesystem.isDirectory(path) then
            send(21) -- EISDIR
          else
            local buffer = ""
            if length ~= 0 then
              local file, err = io.open(path, "rb")
              if file == nil then
                print("Truncate, path not found", err, path, data)
                send(2) -- ENOENT
                goto commandEnd
              else
                while #buffer < length do
                  buffer = buffer .. file:read(length - #buffer)
                end
              end
              file:close()
            end
  
            local file = io.open(path, "w")
            if length ~= 0 then
              file:write(buffer)
            end
            file:close()
            send(0)
          end
        elseif command == "write" then
          local offset = decode_int(data)
          local endIndex = #data
          for i=9, #data do
            if string.sub(data, i, i) == "\0" then
              endIndex = i-1
              break
            end
          end
          local path = string.sub(data, 9, endIndex)
          local buffer = string.sub(data, endIndex + 2, #data)
          local file = io.open(path, "a")
          if file == nil then
            send(2) -- File not found
          else
            if offset ~= 0 then
              print("Seeking write", offset, file:seek("set", offset))
            end
            local ok, err = file:write(buffer)
            if not ok then
              print("Not ok", err)
            end
            file:close()
            --local written = 0
            --[[while written < #buffer do
              local 
            end--]]
            if ok then
              send(0)
            else
              send(5) -- EIO
            end
          end
        elseif command == "unlink" then
          local path = data
          if filesystem.isDirectory(path) then
            send(21) -- EISDIR
          else
            local success, err = filesystem.remove(path)
            if success then
              send(0)
            else
              send(2) -- ENOENT
            end
          end
        elseif command == "rmdir" then
          local path = data
          if not filesystem.exists(path) then
            send(2) -- ENOENT
          elseif not filesystem.isDirectory(path) then
            send(20) -- ENOTDIR
          elseif filesystem.list(path)() == nil then -- Empty
            filesystem.remove(path)
            send(0)
          else
            send(39) -- ENOTEMPTY
          end
        elseif command == "rename" then
          local endIndex = nil
          for i=1,#data do
            if string.sub(data, i, i) == "\0" then
              endIndex = i - 1
              break
            end
          end
          local old = string.sub(data, 1, endIndex)
          local new = string.sub(data, endIndex + 2)
          local ok, err = filesystem.rename(old, new)
          if ok then
            send(0)
          else
            print("Rename failed", old, new, err)
            send(2) -- ENOENT
          end
        else
          print("Unknown command: " .. command)
        end
        ::commandEnd::
      end
      client.close()
      clients[clientIndex] = nil
      print("Client bailing out!")
    end, client, clientIndex)
    ::continue::
  end
  print("Exiting!")
  for _, client in pairs(clients) do
    print("Closing client!")
    client.close()
  end
  print("Closing server!")
  server.close()
  print("Exited. Have a nice day c:")
end)