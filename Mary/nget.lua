local component = require("component")
local fs = require("filesystem")
local internet = require("internet")
local shell = require("shell")
local text = require("text")
local term = require("term")
local computer = require("computer")
local downloaded = 0
if not component.isAvailable("internet") then
  io.stderr:write("This program requires an internet card to run.")
  return
end

local args, options = shell.parse(...)
options.q = options.q or options.Q

if #args < 1 then
  io.write("Usage: wget [-fq] <url> [<filename>]\n")
  io.write(" -f: Force overwriting existing files.\n")
  io.write(" -q: Quiet mode - no status messages.\n")
  io.write(" -Q: Superquiet mode - no error messages.")
  return
end

local url = text.trim(args[1])
local filename = args[2]
local function parseUrl(filename)
  local index = string.find(filename, "/[^/]*$")
  if index then
    filename = string.sub(filename, index + 1)
  end
  index = string.find(filename, "?", 1, true)
  if index then
    filename = string.sub(filename, 1, index - 1)
  end
  return filename
end
if not filename then
  filename = parseUrl(url)
end
filename = text.trim(filename)
if filename == "" then
  if not options.Q then
    io.stderr:write("could not infer filename, please specify one")
  end
  return nil, "missing target filename" -- for programs using wget as a function
end
filename = shell.resolve(filename)

local preexisted
if fs.exists(filename) then
  preexisted = true
  if not options.f then
    if not options.Q then
      io.stderr:write("file already exists")
    end
    return nil, "file already exists" -- for programs using wget as a function
  end
end

local f, reason = io.open(filename, "a")
if not f then
  if not options.Q then
    io.stderr:write("failed opening file for writing: " .. reason)
  end
  return nil, "failed opening file for writing: " .. reason -- for programs using wget as a function
end
f:close()
f = nil

local function sizestring(size)
  if ( size <= 0 ) then return "0" end
  if ( size < 1024 ) then return size .. "B" end
  -- Switch to MB after 5 KB
  if ( size < 1024 * 1024 * 5) then return string.format("%.2f", size / 1024) .. "K" end
  if ( size < 1024 * 1024 * 1024 ) then return string.format("%.2f", size / (1024 * 1024)) .. "M" end

  return string.format("%.2f", size / ( 1000 * 1000 * 1000 )) .. "G"
end

if not options.q then
  io.write("Downloading...\n ")
end
local start = computer.uptime()
local result, response = pcall(internet.request, url)
if result then
  local length = 0
  local result, reason = pcall(function()
    while true do
      local collected = 0
      for i=1,512 do
        chunk = response.read()
        if chunk == nil then goto stop end
        if #chunk > 0 then
          downloaded = downloaded + #chunk
          collected = collected + #chunk
          --print("Chunk:  " .. downloaded,"bytes +" .. #chunk)
          if not f then
            f, reason = io.open(filename, "wb")
            assert(f, "failed opening file for writing: " .. tostring(reason))
            local mt = getmetatable(response)
            local code, message, headers = mt.__index.response()
            if headers["Content-Length"] ~= nil then
              local header = headers["Content-Length"]
              local collector = ""
              for i=1,header["n"] do
                collector = collector .. header[i]
              end
              length = tonumber(collector)
            end
          end
          f:write(chunk)
          local x,y = term.getCursor()
          term.setCursor(1,y)
          local w = term.getViewport()
          local prefix = parseUrl(filename) .. " ["
          local suffix = "]  " .. sizestring(downloaded)
          if not options.l then
            suffix = suffix .. "/" .. sizestring(length)
            local time = computer.uptime()-start
            suffix = suffix .. " " .. sizestring(downloaded/time) .. "/s"
          end
          local progLen = w-string.len(prefix)-string.len(suffix)
          local progress = ""
          local filled = math.floor(progLen*(downloaded/length))
--          print("Filling" .. filled, progLen*(downloaded/length))
          for i=1,filled-1 do
            progress = progress .. "="
          end
          if filled >= 1 then
            progress = progress .. ">"
          end
          for i=1,progLen-filled do
            progress = progress .. " "
          end
          term.write(prefix .. progress .. suffix)
        end
      end
      os.sleep(0)
    end
    ::stop::
  end)
  if not result then
    if not options.q then
      io.stderr:write("failed.\n")
    end
    if f then
      f:close()
      if not preexisted then
        fs.remove(filename)
      end
    end
    if not options.Q then
      io.stderr:write("HTTP request failed: " .. reason .. "\n")
    end
    return nil, reason -- for programs using wget as a function
  end
  if f then
    f:close()
  end

  if not options.q then
    io.write("Saved data to " .. filename .. " (" .. sizestring(downloaded) .. ")\n")
  end
else
  if not options.q then
    io.write("failed.\n")
  end
  if not options.Q then
    io.stderr:write("HTTP request failed: " .. response .. "\n")
  end
  return nil, response -- for programs using wget as a function
end
return true -- for programs using wget as a function