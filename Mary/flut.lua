local component = require("component")
local gpu = component.gpu
local internet = require("internet")
local term = require("term")
local event = require("event")

gpu.setDepth(8)


local sock = internet.open("adryd.com", 6969)
sock:write("Ready for data please\n")
print("Socky")

local w, h = gpu.getResolution()
gpu.fill(1, 1, w, h, " ") -- clears the screen

local function startswith(source, query)
  if string.sub(source, 1, string.len(query)) == query then
    return true
  else return false end
end

local function getcoords(val)
  local x = ""

  for i=1,string.len(val) do
    local chr = string.sub(val,i,i)
    if chr == " " then
      local numX = tonumber(x)
      local numY = tonumber(string.sub(val, i+1))
      if numX == nil or numY == nil then return nil end
      return math.floor(1+numX), math.floor(1+numY)
    else
      x = x .. chr
    end
  end
end

local scr = {}
for i=1,w do
  scr[i] = {}
  for j=1,h*2 do
    scr[i][j] = 0
  end
end

local ctr = 0
local buf = ""
local function tick()
  local chr = sock:read(1)
  if chr == "" or chr == nil then
    if ctr > 2^64 then
      os.sleep(0)
      ctr = 0
    end
    ctr = ctr + 1
    return
  end

  -- term.write(chr)

  if chr == "\n" then
    if startswith(buf, "PX ") then
      local x, y = getcoords(string.sub(buf, 4, string.len(buf) - 7))
      if x == nil or y == nil or x < 1 or y < 1 or x > w or y > (h*2) then
        buf = ""
        return
      end
      local colstr = string.sub(buf, string.len(buf) - 5)
      local color = tonumber("0x" .. colstr)
      gpu.set(1, 1, tostring(x) .. "," .. tostring(y))
      if color == nil then
        buf = ""
        return
      end

      scr[x][y] = color
      if y % 2 == 1 then
        gpu.setForeground(color, false)
        gpu.setBackground(scr[x][y+1], false)
      else
        gpu.setForeground(scr[x][y-1], false)
        gpu.setBackground(color, false)
      end

      gpu.set(x, math.floor((y+1)/2), "▀")
    else
      -- print("Unknown, throwing out " .. buf)
    end
    buf = ""
  else
    buf = buf .. chr
  end
end

while true do
  tick()

--  for i=0,2^18 do
--    tick()
--  end
--  os.sleep(0) -- yield
end