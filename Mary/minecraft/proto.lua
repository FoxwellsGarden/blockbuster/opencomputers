local bit32 = require("bit32")
function p_varint(value)
  if value == 0 then return '\0' end
  local collector = ""
  while value ~= 0 do
    local temp = bit32.band(value, 127)
    value = bit32.rshift(value, 7)
    if value ~= 0 then
      temp = bit32.bor(temp, 128)
    end
    collector = collector .. string.char(temp)
  end
  return collector
end

function p_string(value)
  return p_varint(#value) .. value
end

function p_short(value)
  return string.char(bit32.rshift(value, 8)) .. string.char(bit32.band(255))
end

function r_varint(sock)
  local result = 0
  local shift = 0
  while true do
    local b = nil
    while b == nil do b = string.byte(sock.read(1)) end
    result = bit32.bor(result, bit32.lshift(bit32.band(b, 0x7f), shift))
    if bit32.band(b, 0x80) == 0 then return result end
    shift = shift + 7
    if shift >= 64 then return print("FART") end
  end
end

function r_varint2(sock)
  local numRead = 0
  local result = 0
  local read = 0xff
  while bit32.band(read, 128) ~= 0 do
    read = nil
    while read == nil do
      read = string.byte(sock.read(1))
      os.sleep()
    end
    local value = bit32.band(read, 127)
    result = bit32.bor(bit32.lshift(value, 7*numRead))
    numRead = numRead + 1
    if numRead > 5 then
      print("Angy mode")
    end
  end
  return result
end

function r_string(sock)
  local size = r_varint(sock)
  local collector = ""
  while #collector < size do
    collector = collector .. sock.read(size)
  end
  return collector
end