local net = require("component").internet
local term = require("term")
require("./proto")
local json = require("./json")
local base64 = require("./base64")
local png = require("png")
local gpu = require("component").proxy("4a6c475f-5751-4d4f-97d7-872725005d02")
gpu.bind("44dc9033-63e6-4753-b3a0-7e08a9f20467")
local gpu2 = require("component").proxy("359c60cc-72dc-4f48-a8d4-f00560ca6c85")
gpu2.bind("1fd4eb46-55bd-4eaf-8b2c-c6f6bba08732")
local bit32 = require("bit32")

while true do
local sock = net.connect("mc.hypixel.net", 25565)
local function send(id, data)
  local frame = p_varint(id) .. data
  local packet = p_varint(#frame) .. frame
  --print(packet)
  sock.write(packet)
end

sock.finishConnect()
gpu.setResolution(64,32)

send(0, p_varint(340) .. p_string("mc.hypixel.net") .. p_short(25565) .. p_varint(1))
send(0,"")

local length = r_varint(sock)
local id = r_varint(sock)
if id ~= 0 then print("Poop the shit" .. id) end
local data = json.decode(r_string(sock))

local faviconPrefix = #"data:image/png;base64,"
local file,error = require("io").open("/tmp/favicon.png", "ab")
if file == nil then
  print("Uh-oh", error, file)
end
local faviconData = base64.decode(string.sub(data.favicon, faviconPrefix))
file:write(base64.decode(string.sub(data.favicon, faviconPrefix)))
file:close()
local file = require("io").open("/tmp/favicon.png", "rb")
local image = png(file)
file:close()
for x=1,image.width do
  for y=0,math.floor(image.height/2)-1 do
    local yCoord = (y*2)+1
    local top = image.pixels[yCoord][x]
    local bottom = image.pixels[yCoord+1][x]
    local topColor = top.R*0x10000 + top.G*0x100 + top.B
    local bottomColor = bottom.R*0x10000 + bottom.G*0x100 + bottom.B
    gpu.setForeground(topColor)
    gpu.setBackground(bottomColor)
    gpu.set(x, y+1, "▀")
  end
end

local colors = {
  ["0"] = 0,
  ["1"] = 0x0000aa,
  ["2"] = 0x00aa00,
  ["3"] = 0x00aaaa,
  ["4"] = 0xaa0000,
  ["5"] = 0xaa00aa,
  ["6"] = 0xffaa00,
  ["7"] = 0xaaaaaa,
  ["8"] = 0x555555,
  ["9"] = 0x5555ff,
  ["a"] = 0x55ff55,
  ["b"] = 0x55ffff,
  ["c"] = 0xff5555,
  ["d"] = 0xff55ff,
  ["e"] = 0xffff55,
  ["f"] = 0xffffff,
}

local function embolden(color)
  return (bit32.band(0xff, bit32.rshift(color, 16) * 10) * 0x10000) +
    (bit32.band(255, bit32.band(bit32.rshift(color, 8), 0xff) * 10) * 0x100) +
    bit32.band(bit32.band(color, 0xff) * 10, 0xff)
end

local motd = data.description
if motd.text then motd = motd.text end
local motdData = {}
local x = 1
local y = 1
local bold = false
local i = 1
local w, h = gpu2.getResolution()
gpu2.fill(1, 1, w, h, " ")
while i <= #motd do
  local char = string.sub(motd, i, i)
  local escape = string.sub(motd, i, i+1)
  if escape == "\u{00a7}" then
    i = i + 2
    local code = string.sub(motd, i, i)
    if code == "l" then
      bold = true
      gpu2.setForeground(embolden(gpu2.getForeground()))
    elseif code == "k" then
    elseif code == "m" then
    elseif code == "n" then
    elseif code == "o" then
    elseif code == "r" then
      bold = false
      gpu.setForeground(0xffffff)
      gpu.setBackground(0)
    else
      gpu2.setForeground(colors[code])
      --print("Foreground", colors[code])
      if bold then
        gpu2.setForeground(embolden(colors[code]))
      end
    end
  elseif char == "\n" then
    y = y + 1
    x = 1
  else
    gpu2.set(x, y, char)
    x = x + 1
  end
  i = i + 1
end
y = y + 1
local w = x-1
local players = data.players.online .. " / " .. data.players.max
gpu2.set(1 + (w/2) - (#players/2), y, players)
gpu2.setResolution(x-1, y)
sock.close()
os.sleep(0)
end
