local component = require("component")
local shell = require("shell")
local io = require("io")
local filesystem = require("filesystem")
local server, error = component.compute_card.bind(5101)
if server == nil then
  print(error)
  return
end

local config = {
  HEADER_SIZE = 8192, -- Header buffer size
  MAX_NEW_CONNECTIONS = 16, -- New connections per tick
  ROOT_DIR = "/mnt/6af/", -- Location for files
  NOT_FOUND_PAGE = "/404.html", -- 404 page
  CHUNK_SIZE = 8192, -- Read files in N byte segments
  CONNECTION_TICKS = 16, -- Ticks of connection per tick of loop
}

local connections = {}
local reasonPhrases = {
  [200] = "OK",
  [500] = "Internal Server Error",
  [404] = "Not Found",
}

local mimes = {
  html = "text/html",
  js = "application/javascript",
  txt = "text/plain",
  png = "image/png",
}

local function send(connIdx, conn, code, data)
  res = "HTTP/1.1 " .. tostring(code) .. " " .. reasonPhrases[code] .. "\r\n"
  res = res .. "Server: funny\r\n"
  if data ~= nil then
    res = res .. "Content-Length: " .. #data .. "\r\n"
    res = res  .. "\r\n" .. data
  end

  local res, err = conn.sock.write(res)
  if data ~= nil or err ~= nil then
    connections[connIdx] = nil
  end
  return res, err
end

local function sendMime(conn, path)
  -- Add Content-Length from fs
  local _, err = conn.sock.write("Content-Length: " .. math.floor(filesystem.size(path)) .. "\r\n")
  if err ~= nil then
    return _, err
  end
  for i=#path,1,-1 do
    local char = string.sub(path, i, i)
    if char == "." then
      local type = string.sub(path, i+1, #path)
      local mime = mimes[type]
      if mime == nil then
        return
      end
      local _, err = conn.sock.write("Content-Type: " .. mime .. "\r\n")
      return _, err
    end
  end
end

-- Checks if we're at the end of the request buffer
local function checkEndHeaders(value)
  return string.sub(value,#value - 3) == "\r\n\r\n" or string.sub(value,#value - 1) == "\n\n"
end

local function readFile(path, conn)
  local filePath = config.ROOT_DIR .. shell.resolve("/" .. path)
  local file, error = io.open(filePath, "rb")
  if file == nil then
    return nil
  end
  conn.file = file
  conn.filePath = filePath
  return file
end

local function fileChunk(connIdx, conn)
  local file = conn.file
  local data, error = file:read(config.CHUNK_SIZE)
  if data == nil then
    print("File close")
    file:close()
    conn.file = nil
    conn.sock.close()
    conn.sock = nil
    connections[connIdx] = nil
    if error then
      print("Error reading", error)
      return nil
    end
    return nil
  end
  if conn.sent == nil then
    conn.sent = true
    data = "\r\n" .. data
  end

  local _, err = conn.sock.write(data)
  if err ~= nil then
    file:close()
    conn.file = nil
    conn.sock.close()
    conn.sock = nil
    connections[connIdx] = nil
    return
  end
  return true
end

local function table_tostring(table)
  local response = ""
  for key,value in pairs(table) do
    local t = type(value)
    local stringValue = ""
    if t == "table" then
      stringValue = table_tostring(table)
    elseif t == "string" then
      stringValue = '"' .. value .. '"'
    elseif t == "number" then
      stringValue = tostring(value)
    else
      assert(false)
    end
    response = response .. '"' .. key .. '" = ' .. stringValue .. ",\n"
  end
  return response
end

local function handleResponse(connIdx, conn)
  if conn.path == "/ipTest" then
    local address, port = conn.sock.getAddress()
    local headers = table_tostring(conn.headers)
    return send(connIdx, conn, 200, "Your ip is: " .. address .. ":" .. math.floor(port) .. "\n" .. headers)
  end
  local files = {
    [2] = conn.path,
    [1] = conn.path.."/index.html"
  }
  local file = nil
  while file == nil do
    local attempt = table.remove(files)
    if attempt == nil then
      break
    end
    file = readFile(attempt, conn)
  end
  if file == nil then
    file = readFile(config.NOT_FOUND_PAGE, conn)
    if file == nil then
      return send(connIdx, conn, 404, "Not Found")
    end
    -- Send headers
    local _, err = send(connIdx, conn, 404, nil)
    if err == nil then
      return
    end
    -- Send Content-Type and Content-Length
    _, err = sendMime(conn, conn.filePath)
    if err == nil then
      connections[connIdx] = nil
      return
    end
    fileChunk(connIdx, conn)
  else
    -- Send headers
    send(connIdx, conn, 200, nil)
    sendMime(conn, conn.filePath)
    fileChunk(connIdx, conn)
  end
end

local function handleConnection(connIdx, conn)
  if conn.headers == nil then
    -- Someone could exhaust our resources!
    while #conn.buffer < config.HEADER_SIZE do
      local char = conn.sock.read(config.HEADER_SIZE-#conn.buffer)
      -- No more data available
      if char == "" or char == nil then break end
      conn.buffer = conn.buffer .. char
      if checkEndHeaders(conn.buffer) then
        local idx = 1
        for i=1,#conn.buffer do
          if string.sub(conn.buffer, i, i) == " " then
            conn.method = string.sub(conn.buffer, 0, i - 1)
            idx = i+1
            break
          end
        end
        -- Path
        for i=idx,#conn.buffer do
          if string.sub(conn.buffer, i, i) == " " then
            conn.path = string.sub(conn.buffer, idx, i-1)
            idx = i+1
            break
          end
        end
        print("<- " .. conn.method .. " " .. conn.path)
        -- Check that version is correct
        if string.match(string.sub(conn.buffer,idx),"^HTTP/1.1") == nil then
          print("Version:",string.sub(conn.buffer,idx))
          send(connIdx, conn, 500, "Unsupported Protocol Version")
          return 1
        end
        idx = idx + #"HTTP/1.1"
        if string.sub(conn.buffer,idx,idx) == "\r" then
          idx = idx + 1
        end
        if string.sub(conn.buffer,idx,idx) == "\n" then
          idx = idx + 1
        else
          send(connIdx, conn, 500, "Missing newlines")
          return 1
        end
        -- Headers
        conn.headers = {}
        local key = ""
        local value = nil
        local i = idx - 1
        while i <= #conn.buffer do
          i = i + 1 -- Janky, see -1 above
          local char = string.sub(conn.buffer,i,i)
          if char == "\r" and string.sub(conn.buffer,i+1,i+1) == "\n" then
            -- Do nothing, \r\n
          elseif char == "\n" then
            -- Next header!
            if key == "" then
              -- We've reached the request body
              break
            else
              -- Lowercased key as this seems to be the convention
              conn.headers[string.lower(key)] = value
              key = ""
              value = nil
            end
          elseif char == ":" then
            value = ""
            -- Skip spaces after header key
            for j=i+1,#conn.buffer do
              if string.sub(conn.buffer,j,j) ~= " " then
                i = j-1
                break
              end
            end
          else
            if value == nil then
              key = key .. char
            else
              value = value .. char
            end
          end
        end
        --send(connIdx, conn, 200, "hi there")
        handleResponse(connIdx, conn)
        return 1
      end
    end
  elseif conn.file ~= nil then
    fileChunk(connIdx, conn)
  end
end

local function listen()
  -- Grab new connections
  for i=1,config.MAX_NEW_CONNECTIONS do
    local socket = server.accept()
    if socket == nil then
      break
    end
    --print("New connection!")
    table.insert(connections, {
      buffer = "",
      sock = socket
    })
  end
  -- Run events!
  for _=1,config.CONNECTION_TICKS do
    if #connections == 0 then
      break
    end
    for i, conn in pairs(connections) do
      --print("Ticking connection", i)
      handleConnection(i, conn)
    end
  end
end

while true do
  listen() -- Tick event loop
  os.sleep(0)
end