local component = require("component")
local server = component.compute_card.bind(5170)
local coroutine = require("coroutine")
local shell = require("shell")
local io = require("io")

local config = {
  MAX_NEW_CONNECTIONS = 16,
  BUFFER_READ_SIZE = 512, -- Bytes to read per coro
  BASE_DIR = "/mnt/5d2/", -- Location of indexes
  CHUNK_SIZE = 512, -- Bytes to read per chunk
}

-- Array of coroutines
local coroutines = {}
local function processEvents()
  for i=1,config.MAX_NEW_CONNECTIONS do
    local client = server.accept()
    if client == nil then
      break
    end
    local session = {
      buffer = "",
    }
    -- Read loop
    local coro = coroutine.create(function()
      while true do
        local tmp, err = client.read(config.BUFFER_READ_SIZE)
        if tmp == "" or tmp == nil then
          coroutine.yield() -- Yield because no data in buffer
        end
        local startIdx = #session.buffer
        session.buffer = session.buffer .. tmp
        for i=startIdx,#session.buffer-1 do
          if string.sub(session.buffer,i,i) == "\r" and string.sub(session.buffer,i+1,i+1) == "\n" then
            session.path = string.sub(session.buffer, 1, i-1)
            break
          end
        end
        if session.path == nil then
          coroutine.yield()
        else
          break
        end
      end
      -- Path set!
      if session.path == "" then
        session.path = "index"
      end
      print("<- " .. session.path)
      local handle = io.open(config.BASE_DIR .. shell.resolve("/" .. session.path), "rb")
      local data = nil
      if handle == nil then
        client.write("3'" .. session.path .. "' does not exist\terror.host\t1")
      end
      if handle ~= nil then
        while true do
          local chunk = handle:read(config.CHUNK_SIZE)
          if chunk == "" then
            coroutine.yield() -- No more buffered data, yield
          elseif chunk == nil then
            break -- End of file
          end
          client.write(chunk)
          coroutine.yield()
        end
      end
      -- End of file
      client.write("\r\n")
      client.close()
      if handle ~= nil then
        handle:close()
      end
      return true
    end)
    table.insert(coroutines, coro)
  end
  for key, coro in pairs(coroutines) do
    local success, res = coroutine.resume(coro)
    if success == false then
      error("Didn't succeed: " .. res)
    end

    if res == true then
      coroutines[key] = nil
    end
  end
end

while true do
  processEvents()
  os.sleep(0)
end