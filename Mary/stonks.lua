local component = require("component")
local gpu2 = component.proxy("d4565e7b-fc2f-4a16-8f46-e7e45a56526e")
local success, error = gpu2.bind("15e7ea05-5b4a-4d70-ab0f-82186e9df3ce")
local gpu = component.gpu
local internet = require("internet")
local json = require("json")
local ibm = require("./ibm")

gpu2.setResolution(30,50)

local screen2Width, screen2Height = gpu2.getResolution()
local screenWidth, screenHeight = gpu.getResolution()
local char = "█"
local interpolX = 5

local function set(x, y, char)
  gpu.set(x, screenHeight - y, char)
end
local function clear()
  gpu.fill(1, 1, screenWidth, screenHeight, " ")
  gpu2.fill(1, 1, screen2Width, screen2Height, " ")
end

local function plot(points)
  local maxY = 0
  local minY = math.huge
  for index, point in ipairs(points) do
    if point.y < minY then
      minY = point.y
    elseif point.y > maxY then
      maxY = point.y
    end
  end
  local scaleY = screenHeight / (maxY - minY)
  local interpolX = math.floor(screenWidth / #points)
--  print("scaleY = " .. scaleY)

  local screenX = 1
  for index, point in ipairs(points) do
    local next = points[index+1]
    gpu.setForeground(0xffffff)
--    print("Real coordinates " .. point.x .. "," .. point.y)
--    print("lowest y " .. point.y - minY)
--    print("screen y should be " .. (point.y - minY)*scaleY)
    set(screenX, (point.y-minY)*scaleY, char)
    if next ~= nil then
      local slope = ((point.y - next.y)/(point.x-next.x))/interpolX
      --print(slope)
      if next.y < point.y then
        gpu.setForeground(0xff0000)
      else
        gpu.setForeground(0x00ff00)
      end
      --print("x=" .. point.x .. ",y=" .. point.y .. "  NEXT  x=" .. next.x .. ",y=" .. next.y)
      local y = (slope + point.y) - minY
      for diff=1,interpolX do
        screenX = screenX + 1
        y = y + slope
--        print("x = " .. screenX / interpolX .. " y = " .. y .. " slop = " .. slope .. " real scaleY = " .. y * scaleY)
        --print(screenX .. " y = " .. y * scaleY)
        set(screenX, math.floor(y * scaleY), char)
      end
    end
    screenX = screenX + 1
  end
end

--[[ plot({
  [1] = {
    x = 1,
    y = 4
  },
  [2] = {
    x = 2,
    y = 1
  },
  [3] = {
    x = 3,
    y = 5
  },
  [4] = {
    x = 4,
    y = 30
  }
}) ]]

local function drawHistory(history)
  local index = 1
  for idx, candle in ipairs(history) do
    gpu2.set(1, idx, os.date("%x", candle.time / 1000) .. " $" .. candle.money)
  end
  gpu2.setBackground(0x1f70c1)
--  ibm(screen2Width, gpu2.set)
end

local url = "https://www.investing.com/common/modules/js_instrument_chart/api/data.php?pair_id=8082&pair_id_for_news=8082&chart_type=area&pair_interval=86400&candle_count=30&events=yes&volume_series=yes"
local headers = {
  ["X-Requested-With"] = "XMLHttpRequest",
  ["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4259.3 Safari/537.36",
  ["Referer"] = "https://www.investing.com/equities/ibm"
}
local function update()
  local request = internet.request(url, nil, headers)
  local serialised = ""
  for chunk in request do
    serialised = serialised .. chunk
  end
  local data = json.decode(serialised)
  local points = {}
  local history = {}
  for idx, candle in ipairs(data.candles) do
    local time = candle[1]
    local money = candle[2]
    history[idx] = {
      money = money,
      time = time
    }
    if idx > (#data.candles)-20 then
      points[idx-(#data.candles)+20] = {
        x = idx,
        y = money
      }
    end
  end
  clear()
  plot(points)
  drawHistory(history)
end

while true do
  update()
  os.sleep(60 * 60 * 4)
end